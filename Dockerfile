FROM maven:3.6.3-openjdk-11
WORKDIR /root/
COPY src .
COPY pom.xml .
RUN ["mvn", "install"]
ENTRYPOINT ["java",  "-jar",  "target/kelevra-1.0-SNAPSHOT-jar-with-dependencies.jar"]
EXPOSE 39084