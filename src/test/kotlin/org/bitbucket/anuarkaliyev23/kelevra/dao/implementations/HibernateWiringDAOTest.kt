package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import org.junit.jupiter.api.Test

import java.time.LocalDate

internal class HibernateWiringDAOTest {

    @Test
    fun findAll() {
        val wiring = Wiring(
                credit = HibernateAccountDAO.findById("1.1"),
                debit = HibernateAccountDAO.findById("1"),
                sender = HibernateWiringPartyDAO.findById("myself"),
                receiver = HibernateWiringPartyDAO.findById("myself"),
                amount = 10000,
                date = LocalDate.now(),
                note = "Тестовая запись"
        )
        HibernateWiringDAO.save(wiring)
        val all = HibernateWiringDAO.findAll()
        all.forEach {
            println(it.debit)
            println(it.credit)
        }
        val wirings = HibernateWiringDAO.findByCredit(HibernateAccountDAO.findById("1.1")!!)
        println(wirings)
    }
}