package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.junit.jupiter.api.Test

internal class HibernateWiringPartyDAOTest {

    @Test
    fun findAll() {
        val party1 = GenericWiringParty(
                id = "myself",
                description = "Дусалиев Хамбар"
        )
        HibernateWiringPartyDAO.save(party1)

        println(HibernateWiringPartyDAO.findAll())

    }
}