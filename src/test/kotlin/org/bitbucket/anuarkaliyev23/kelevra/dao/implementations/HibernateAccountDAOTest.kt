package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.junit.jupiter.api.Test

internal class HibernateAccountDAOTest {
    @Test
    fun findAll() {

        val account1 = Account(
                id = "1",
                description = "Краткосрочные активы",
                active = true,
                parent = null
        )

        val account2 = Account(
                id = "1.1",
                description = "Деньги",
                active = true,
                parent = account1
        )

        HibernateAccountDAO.save(account1)
        HibernateAccountDAO.save(account2)

        HibernateAccountDAO.findAll().forEach { println(it) }
    }

    @Test
    fun findById() {
        println(HibernateAccountDAO.findById("1.1"))
    }

}