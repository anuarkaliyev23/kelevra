package org.bitbucket.anuarkaliyev23.kelevra.model.persistence

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring.Companion.DB_TABLE_NAME
import java.time.LocalDate
import javax.persistence.*

@Entity(name = DB_TABLE_NAME)
data class Wiring(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = DB_ID_FIELD_NAME)
        var id: Int = 0,

        @ManyToOne
        @JoinColumn(name = DB_DEBIT_FIELD_NAME, referencedColumnName = Account.DB_ID_FIELD_NAME, nullable = false)
        var debit: Account? = null,

        @ManyToOne
        @JoinColumn(name = DB_CREDIT_FIELD_NAME, referencedColumnName = Account.DB_ID_FIELD_NAME, nullable = false)
        var credit: Account? = null,

        @ManyToOne
        @JoinColumn(name = DB_SENDER_FIELD_NAME, referencedColumnName = GenericWiringParty.DB_ID_FIELD_NAME)
        var sender: GenericWiringParty? = null,

        @ManyToOne
        @JoinColumn(name = DB_RECEIVER_FIELD_NAME, referencedColumnName = GenericWiringParty.DB_ID_FIELD_NAME)
        var receiver: GenericWiringParty? = null,

        @Column(name = DB_AMOUNT_FIELD_NAME)
        var amount: Long = 0,

        @Column(name = DB_DATE_FIELD_NAME, nullable = false)
        var date: LocalDate = LocalDate.now(),

        @Column(name = DB_NOTE_FIELD_NAME, nullable = false)
        var note: String = "",

        @Embedded
        var meta: ModelMetaInfo = ModelMetaInfo()
) {
        companion object {
                const val DB_TABLE_NAME = "wirings"
                const val DB_ID_FIELD_NAME = "wiring_id"
                const val DB_DEBIT_FIELD_NAME = "debit"
                const val DB_CREDIT_FIELD_NAME = "credit"
                const val DB_SENDER_FIELD_NAME = "sender"
                const val DB_RECEIVER_FIELD_NAME = "receiver"
                const val DB_AMOUNT_FIELD_NAME = "amount"
                const val DB_DATE_FIELD_NAME = "date"
                const val DB_NOTE_FIELD_NAME = "note"
        }

    override fun toString(): String {
        return "Wiring(id=$id, " +
                "debit=${debit?.id}, " +
                "credit=${credit?.id}, " +
                "sender=$sender, " +
                "receiver=$receiver, " +
                "amount=$amount, " +
                "date=$date, " +
                "note='$note', " +
                "meta=$meta)"
    }

}