package org.bitbucket.anuarkaliyev23.kelevra.model.persistence

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class ModelMetaInfo {
    @Column(updatable = false)
    var createdAt: LocalDateTime = LocalDateTime.now()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ModelMetaInfo

        if (createdAt != other.createdAt) return false

        return true
    }

    override fun hashCode(): Int {
        return createdAt.hashCode()
    }

    override fun toString(): String {
        return "ModelMetaInfo(createdAt=$createdAt)"
    }


}