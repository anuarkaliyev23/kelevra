package org.bitbucket.anuarkaliyev23.kelevra.model.persistence

import org.bitbucket.anuarkaliyev23.kelevra.model.WiringParty
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty.Companion.DB_TABLE_NAME
import javax.persistence.*


@Entity(name = DB_TABLE_NAME)
data class GenericWiringParty(
        @Id
        @Column(name = DB_ID_FIELD_NAME)
        var id: String = "",

        @Column(name = DB_DESCRIPTION_FIELD_NAME, nullable = false)
        var description: String = "",

        @Embedded
        var meta: ModelMetaInfo = ModelMetaInfo()
) : WiringParty {

    override fun identifier(): String = id
    companion object {

        const val DB_TABLE_NAME = "parties"
        const val DB_ID_FIELD_NAME = "id"
        const val DB_DESCRIPTION_FIELD_NAME = "description"
    }
}