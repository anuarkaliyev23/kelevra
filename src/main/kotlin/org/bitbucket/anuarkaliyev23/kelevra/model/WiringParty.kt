package org.bitbucket.anuarkaliyev23.kelevra.model

interface WiringParty {
    fun identifier() : String
}