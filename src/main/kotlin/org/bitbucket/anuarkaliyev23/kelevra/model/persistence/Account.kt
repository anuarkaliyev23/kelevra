package org.bitbucket.anuarkaliyev23.kelevra.model.persistence

import javafx.beans.value.ObservableValue
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account.Companion.DB_TABLE_NAME
import javax.persistence.*

@Entity(name = DB_TABLE_NAME)
data class Account(
        @Id
        @Column(name = DB_ID_FIELD_NAME)
        var id: String = "",

        @Column(name = DB_DESCRIPTION_FIELD_NAME, nullable = false)
        var description: String = "",

        @Column(name = DB_ACTIVE_FIELD_NAME, nullable = false)
        var active: Boolean = false,

        @ManyToOne(cascade = [CascadeType.PERSIST])
        @JoinColumn(name = DB_PARENT_FIELD_NAME, referencedColumnName = DB_ID_FIELD_NAME)
        var parent: Account? = null,

//        @OneToMany(mappedBy = DB_PARENT_FIELD_NAME, fetch = FetchType.EAGER)
//        var children: Set<Account> = HashSet(),

        @Embedded
        var meta : ModelMetaInfo = ModelMetaInfo()
) {
        companion object {
                const val DB_TABLE_NAME = "accounts"
                const val DB_ID_FIELD_NAME = "account_id"
                const val DB_DESCRIPTION_FIELD_NAME = "description"
                const val DB_ACTIVE_FIELD_NAME = "active"
                const val DB_PARENT_FIELD_NAME = "parent_id"
        }
        fun subAccountOf(account: Account): Boolean {
                return (this.id.startsWith(account.id) && this.id != account.id)
        }

        override fun toString(): String {
                return "Account(id=$id, description='$description', active=$active, meta=$meta)"
        }


}