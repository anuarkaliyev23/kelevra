package org.bitbucket.anuarkaliyev23.kelevra.model

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import java.time.LocalDate
import kotlin.collections.ArrayList

data class TurnoverBalanceSheet(
        var account: Account,
        var startBalance: Long,

        var entries: MutableList<Wiring> = ArrayList()
) {
    fun startDate(): LocalDate = entries.minBy { it.date }!!.date
    fun endDate(): LocalDate = entries.maxBy { it.date }!!.date

    //TODO known issue - won't work if subaccount has other type than account
    fun debit(): List<Wiring> = entries.filter { it.debit == account || (it.debit!!.subAccountOf(account) && it.debit!!.active == account.active) }
    fun credit(): List<Wiring> = entries.filter { it.credit == account || it.credit!!.subAccountOf(account) && it.credit!!.active == account.active }

    fun totalBalance(): Long {
        return when (account.active) {
            true -> startBalance + debit().map { it.amount }.sum() - credit().map { it.amount }.sum()
            false -> startBalance + credit().map { it.amount }.sum() - debit().map { it.amount }.sum()
        }
    }
}