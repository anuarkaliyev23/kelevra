package org.bitbucket.anuarkaliyev23.kelevra.services.implementations

import org.bitbucket.anuarkaliyev23.kelevra.LogLevel
import org.bitbucket.anuarkaliyev23.kelevra.configuration.DatabaseUtils
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.KelevraService
import org.bitbucket.anuarkaliyev23.kelevra.web.api.HttpAPI
import org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY
import kotlin.system.exitProcess

object KelevraServiceSimple : KelevraService {
    override fun start(api: HttpAPI) {
        api.run()
    }

    override fun stop() {
        exitProcess(0)
    }


    override fun startDatabase() {
        DatabaseUtils.startDatabase()
    }

    override fun stopDatabase() {
        DatabaseUtils.stopDatabase()
    }

}