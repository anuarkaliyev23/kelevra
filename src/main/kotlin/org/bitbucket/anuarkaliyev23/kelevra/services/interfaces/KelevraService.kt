package org.bitbucket.anuarkaliyev23.kelevra.services.interfaces

import org.bitbucket.anuarkaliyev23.kelevra.LogLevel
import org.bitbucket.anuarkaliyev23.kelevra.web.api.HttpAPI


interface KelevraService {
    fun start(api: HttpAPI)
    fun stop()
    fun startDatabase()
    fun stopDatabase()
}