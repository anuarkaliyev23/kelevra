package org.bitbucket.anuarkaliyev23.kelevra.services.implementations

import org.bitbucket.anuarkaliyev23.kelevra.dao.implementations.HibernateWiringDAO
import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.WiringDAO
import org.bitbucket.anuarkaliyev23.kelevra.exceptions.web.RecordNotFoundException
import org.bitbucket.anuarkaliyev23.kelevra.model.TurnoverBalanceSheet
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.WiringService
import java.time.LocalDate

object WiringServiceSimple : WiringService {
    override val dao: WiringDAO
        get() = HibernateWiringDAO

    override fun findByDebit(account: Account): List<Wiring> = dao.findByDebit(account)
    override fun findByCredit(account: Account): List<Wiring> = dao.findByCredit(account)
    override fun findBySender(sender: GenericWiringParty): List<Wiring> = dao.findBySender(sender)
    override fun findByReceiver(receiver: GenericWiringParty): List<Wiring> = dao.findByReceiver(receiver)
    override fun findAny(wiringParty: GenericWiringParty): List<Wiring> = dao.findAny(wiringParty)
    override fun findAny(account: Account): List<Wiring> = dao.findAny(account)
    override fun findByDate(date: LocalDate): List<Wiring> = dao.findByDate(date)
    override fun findBetween(inclusive: LocalDate, exclusive: LocalDate): List<Wiring> = dao.between(inclusive, exclusive)
    override fun lastEntry(): Wiring = dao.lastEntry() ?: throw RecordNotFoundException()
    override fun findById(id: Int): Wiring = dao.findById(id) ?: throw RecordNotFoundException()

    override fun turnover(account: Account, inclusive: LocalDate, exclusive: LocalDate): TurnoverBalanceSheet {
        val entries = dao.findAny(account).toMutableList()
        entries.sortBy { it.date }
        val entriesBefore = entries.filter { it.date < inclusive }
        val entriesInside = entries.filter { it.date >= inclusive && it.date < exclusive}
        val before = turnoverBalanceSheet(account, entriesBefore.toMutableList())
        val inside = turnoverBalanceSheet(account, entriesInside.toMutableList(), before.totalBalance())
        return inside
    }

    private fun turnoverBalanceSheet(account: Account, entries: MutableList<Wiring>, balance: Long = 0): TurnoverBalanceSheet {
        return TurnoverBalanceSheet(
                account = account,
                startBalance = balance,
                entries = entries
        )
    }


}