package org.bitbucket.anuarkaliyev23.kelevra.services.interfaces

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty

interface WiringPartyService: ModelService<GenericWiringParty, String> {
    override fun findById(id: String): GenericWiringParty
}