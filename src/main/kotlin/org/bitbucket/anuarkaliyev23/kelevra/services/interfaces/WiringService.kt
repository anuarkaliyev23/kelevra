package org.bitbucket.anuarkaliyev23.kelevra.services.interfaces

import org.bitbucket.anuarkaliyev23.kelevra.model.TurnoverBalanceSheet
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import java.time.LocalDate

interface WiringService : ModelService<Wiring, Int> {
    fun findByDebit(account: Account): List<Wiring>
    fun findByCredit(account: Account): List<Wiring>
    fun findBySender(sender: GenericWiringParty): List<Wiring>
    fun findByReceiver(receiver: GenericWiringParty): List<Wiring>
    fun findAny(wiringParty: GenericWiringParty): List<Wiring>
    fun findAny(account: Account): List<Wiring>
    fun findByDate(date: LocalDate): List<Wiring>
    fun findBetween(inclusive: LocalDate, exclusive: LocalDate): List<Wiring>
    fun lastEntry(): Wiring
    override fun findById(id: Int): Wiring
    fun turnover(account: Account, inclusive: LocalDate, exclusive: LocalDate): TurnoverBalanceSheet
}