package org.bitbucket.anuarkaliyev23.kelevra.services.interfaces

import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.DAO

interface ModelService<T, ID_TYPE>: Service {
    val dao: DAO<T>

    fun listAll() : List<T> =dao.findAll()
    fun delete(t: T) = dao.delete(t)
    fun update(t: T) = dao.update(t)
    fun save(t: T) = dao.save(t)
    fun findById(id: ID_TYPE) : T
}