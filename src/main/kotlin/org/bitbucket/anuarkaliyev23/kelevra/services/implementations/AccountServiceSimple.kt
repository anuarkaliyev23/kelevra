package org.bitbucket.anuarkaliyev23.kelevra.services.implementations

import org.bitbucket.anuarkaliyev23.kelevra.dao.implementations.HibernateAccountDAO
import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.AccountDAO
import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.DAO
import org.bitbucket.anuarkaliyev23.kelevra.exceptions.web.RecordNotFoundException
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.AccountService

object AccountServiceSimple : AccountService {
    override val dao: AccountDAO
        get() = HibernateAccountDAO
    override fun findById(id: String): Account = dao.findById(id) ?: throw RecordNotFoundException()
}