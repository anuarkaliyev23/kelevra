package org.bitbucket.anuarkaliyev23.kelevra.services.interfaces

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account

interface AccountService : ModelService<Account, String> {
    override fun findById(id: String): Account
}