package org.bitbucket.anuarkaliyev23.kelevra.services.implementations

import org.bitbucket.anuarkaliyev23.kelevra.dao.implementations.HibernateWiringPartyDAO
import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.WiringPartyDAO
import org.bitbucket.anuarkaliyev23.kelevra.exceptions.web.RecordNotFoundException
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.WiringPartyService

object WiringPartyServiceSimple : WiringPartyService {
    override val dao: WiringPartyDAO
        get() = HibernateWiringPartyDAO

    override fun findById(id: String): GenericWiringParty = dao.findById(id) ?: throw RecordNotFoundException()
}