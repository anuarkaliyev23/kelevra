package org.bitbucket.anuarkaliyev23.kelevra


import org.bitbucket.anuarkaliyev23.kelevra.configuration.DatabaseUtils
import org.bitbucket.anuarkaliyev23.kelevra.exceptions.arg.UnrecognizedArgException
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.KelevraServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.web.api.JavalinAPI
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations.AccountController
import org.slf4j.impl.SimpleLogger


fun main(args: Array<String>) {
    val parsedArgs = parseArgs(args)
    val logLevelArg = parsedArgs.first{ it::class == LogLevelArg::class } as LogLevelArg
    val webArg = parsedArgs.firstOrNull { it::class == WebArg::class } as WebArg?

    System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, logLevelArg.logLevel.level)

//    DatabaseUtils.startDatabase()
    val portNumber = webArg?.portNumber ?: 39084
    val accountController = AccountController()
    val javalinAPI = JavalinAPI(portNumber, accountController)
    KelevraServiceSimple.start(javalinAPI)
}




fun parseArgs(args: Array<String>) : List<Arg> {
    return when {
        args.isEmpty() -> arrayListOf(LogLevelArg(LogLevel.INFO))
        else -> args.map { parseArg(it) }
    }
}

fun parseArg(arg: String) : Arg {
    val type = ArgType.of(arg)
    return when (type) {
        ArgType.WEB -> WebArg(arg.argValue(ArgType.WEB).toInt())
        ArgType.LOG_LEVEL -> LogLevelArg(LogLevel.valueOf(arg.argValue(ArgType.LOG_LEVEL).toUpperCase()))
    }
}

enum class ArgType(val flag: String) {
    WEB("-web:"),
    LOG_LEVEL("-log:");

    companion object {
        fun of(text: String): ArgType = values().firstOrNull { text.startsWith(it) } ?: throw UnrecognizedArgException(text)
    }
}

enum class LogLevel(val level: String) {
    ERROR("error"),
    WARN("warn"),
    INFO("info"),
    DEBUG("debug"),
    TRACE("trace");
}

sealed class Arg(val type: ArgType)

data class WebArg(val portNumber: Int) : Arg(ArgType.WEB)
data class LogLevelArg(val logLevel: LogLevel) : Arg(ArgType.LOG_LEVEL)


private fun String.startsWith(arg: ArgType): Boolean {
    return (this.startsWith(arg.flag))
}

private fun String.argValue(argType: ArgType): String {
    return (this.substring(argType.flag.length))
}