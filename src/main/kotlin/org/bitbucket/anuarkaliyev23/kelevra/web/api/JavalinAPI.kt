package org.bitbucket.anuarkaliyev23.kelevra.web.api

import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import org.bitbucket.anuarkaliyev23.kelevra.exceptions.web.WebException
import org.bitbucket.anuarkaliyev23.kelevra.logger
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations.AccountController
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations.GenericWiringPartyController
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations.TurnoverController
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations.WiringController

class JavalinAPI(
        val port: Int,
        val accountController: AccountController = AccountController(),
        val genericWiringPartyController: GenericWiringPartyController = GenericWiringPartyController(),
        val wiringController: WiringController = WiringController(),
        val turnoverController: TurnoverController = TurnoverController()
) : HttpAPI {
    override fun run() {
        val app = Javalin.create { config ->
            config.defaultContentType = "application/json"
            config.enableCorsForAllOrigins()
            config.prefer405over404 = true
            config.logIfServerNotStarted = true
            config.showJavalinBanner = false
            config.requestLogger { context, ms ->
                this.logger().info("${context.method()} ${context.path()} completed with ${context.status()} in $ms milliseconds")
            }
        }

        app.routes {
            path("accounts") {
                get(accountController::getAll)
                post(accountController::post)
                path(ID_PP) {
                    get{ ctx -> accountController.getOne(ctx, ctx.pathParam(ID_PP)) }
                    patch{ ctx -> accountController.update(ctx, ctx.pathParam(ID_PP)) }
                    delete{ ctx -> accountController.delete(ctx, ctx.pathParam(ID_PP))}
                }
            }

            path("parties") {
                get(genericWiringPartyController::getAll)
                post(genericWiringPartyController::post)
                path(ID_PP) {
                    get { ctx -> genericWiringPartyController.getOne(ctx, ctx.pathParam(ID_PP)) }
                    patch{ ctx -> genericWiringPartyController.update(ctx, ctx.pathParam(ID_PP)) }
                    delete{ ctx -> genericWiringPartyController.delete(ctx, ctx.pathParam(ID_PP)) }
                }
            }


            path("wirings") {
                get(wiringController::getAll)
                post(wiringController::post)
                path(ID_PP) {
                    get { ctx -> wiringController.getOne(ctx, ctx.pathParam(ID_PP).toInt()) }
                    patch{ ctx -> wiringController.update(ctx, ctx.pathParam(ID_PP).toInt()) }
                    delete{ ctx -> wiringController.delete(ctx, ctx.pathParam(ID_PP).toInt()) }
                }
            }

            path("turnover") {
                get(turnoverController::getOne)
            }
        }

        app.exception(WebException::class.java) { e, ctx ->
//            logger().error(e.stackTrace.toString())
            ctx.contentType("text/plain")
            ctx.result(e.message ?: "")
            ctx.status(e.statusCode)

        }
        app.start(port)
    }

    companion object {
        const val ID_PP = ":id"
    }
}