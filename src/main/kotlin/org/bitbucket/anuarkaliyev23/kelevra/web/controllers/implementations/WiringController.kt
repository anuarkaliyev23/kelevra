package org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.javalin.http.Context
import org.bitbucket.anuarkaliyev23.kelevra.json.ObjectMapperFactory
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.WiringPartyServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.WiringServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.ModelService
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.interfaces.ModelController

class WiringController(
        override val mapper: ObjectMapper = ObjectMapperFactory.objectMapper(),
        override val service: ModelService<Wiring, Int> = WiringServiceSimple
) : ModelController<Wiring, Int> {
    override fun post(ctx: Context) {
        val wiring = mapper.readValue<Wiring>(ctx.body())
        service.save(wiring)
        ctx.status(ModelController.HTTP_STATUS_CREATED_201)
    }

    override fun update(ctx: Context, id: Int) {
        val wiring = mapper.readValue<Wiring>(ctx.body())
        wiring.id = id
        service.update(wiring)
        ctx.status(ModelController.HTTP_STATUS_OK_200)
    }


}