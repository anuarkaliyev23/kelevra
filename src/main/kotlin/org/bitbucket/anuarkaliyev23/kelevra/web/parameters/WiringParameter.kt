package org.bitbucket.anuarkaliyev23.kelevra.web.parameters

enum class WiringParameter(override val parameterName: String): HttpParameter {
    FROM_DATE("from"),
    TO_DATE("to"),
    DEBIT_ACCOUNT("debit"),
    CREDIT_ACCOUNT("credit"),
    SENDER_ACCOUNT("sender"),
    RECEIVER_ACCOUNT("receiver")
}