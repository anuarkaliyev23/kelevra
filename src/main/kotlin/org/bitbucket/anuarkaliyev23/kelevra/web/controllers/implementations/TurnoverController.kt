package org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations

import com.fasterxml.jackson.databind.ObjectMapper
import io.javalin.http.Context
import org.bitbucket.anuarkaliyev23.kelevra.json.ObjectMapperFactory
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.AccountServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.WiringServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.AccountService
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.WiringService
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.interfaces.Controller
import org.bitbucket.anuarkaliyev23.kelevra.web.parameters.TurnoverParameter
import org.bitbucket.anuarkaliyev23.kelevra.web.parameters.queryParam
import java.time.LocalDate

class TurnoverController(
        val accountService: AccountService = AccountServiceSimple,
        val wiringService: WiringService = WiringServiceSimple,
        val mapper: ObjectMapper = ObjectMapperFactory.objectMapper()
) : Controller {
    fun getOne(ctx: Context) {
        val from = LocalDate.parse(ctx.queryParam(TurnoverParameter.FROM_DATE))
        val to = LocalDate.parse(ctx.queryParam(TurnoverParameter.TO_DATE))
        val accountId = ctx.queryParam(TurnoverParameter.ACCOUNT_ID)!!
        val account = accountService.findById(accountId)

        val turnover = wiringService.turnover(account, from, to)
        ctx.result(mapper.writeValueAsString(turnover))
    }
}