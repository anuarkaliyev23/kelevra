package org.bitbucket.anuarkaliyev23.kelevra.web.parameters

import io.javalin.http.Context

fun Context.queryParam(parameter: HttpParameter): String? {
    return this.queryParam(parameter.parameterName)
}