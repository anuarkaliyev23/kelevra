package org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.javalin.http.Context
import org.bitbucket.anuarkaliyev23.kelevra.json.ObjectMapperFactory
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.AccountServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.AccountService
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.interfaces.ModelController

class AccountController (
        override val service: AccountService = AccountServiceSimple,
        override val mapper: ObjectMapper = ObjectMapperFactory.objectMapper()
) : ModelController<Account, String> {

    override fun post(ctx: Context) {
        val account = mapper.readValue<Account>(ctx.body())
        service.save(account)
        ctx.status(ModelController.HTTP_STATUS_CREATED_201)
    }

    override fun update(ctx: Context, id: String) {
        val account = mapper.readValue<Account>(ctx.body())
        account.id = id
        service.update(account)
        ctx.status(ModelController.HTTP_STATUS_OK_200)
    }

}