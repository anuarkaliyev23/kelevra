package org.bitbucket.anuarkaliyev23.kelevra.web.parameters

enum class TurnoverParameter(override val parameterName: String) : HttpParameter {
    FROM_DATE("from"),
    TO_DATE("to"),
    ACCOUNT_ID("account")
}