package org.bitbucket.anuarkaliyev23.kelevra.web.controllers.implementations

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.javalin.http.Context
import org.bitbucket.anuarkaliyev23.kelevra.json.ObjectMapperFactory
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.WiringPartyServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.WiringPartyService
import org.bitbucket.anuarkaliyev23.kelevra.web.controllers.interfaces.ModelController

class GenericWiringPartyController (
        override val service: WiringPartyService = WiringPartyServiceSimple,
        override val mapper: ObjectMapper = ObjectMapperFactory.objectMapper()
) : ModelController<GenericWiringParty, String> {


    override fun post(ctx: Context) {
        val wiringParty = mapper.readValue<GenericWiringParty>(ctx.body())
        service.save(wiringParty)
        ctx.status(201)
    }

    override fun update(ctx: Context, id: String) {
        val wiringParty = mapper.readValue<GenericWiringParty>(ctx.body())
        wiringParty.id = id
        service.update(wiringParty)
        ctx.status(200)
    }


}