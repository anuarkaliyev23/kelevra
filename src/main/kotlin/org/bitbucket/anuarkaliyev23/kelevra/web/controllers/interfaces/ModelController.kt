package org.bitbucket.anuarkaliyev23.kelevra.web.controllers.interfaces

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.readValues
import io.javalin.http.Context
import org.bitbucket.anuarkaliyev23.kelevra.logger
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.ModelService

interface ModelController<T, ID_TYPE> : Controller {
    val mapper: ObjectMapper
    val service: ModelService<T, ID_TYPE>

    fun getOne(ctx: Context, id: ID_TYPE) {
        ctx.result(mapper.writeValueAsString(service.findById(id)))
                .status(HTTP_STATUS_OK_200)
    }

    fun getAll(ctx: Context) {
        ctx.result(mapper.writeValueAsString(service.listAll()))
                .status(HTTP_STATUS_OK_200)
    }

    fun post(ctx: Context)
    fun update(ctx: Context, id: ID_TYPE)

    fun delete(ctx: Context, id: ID_TYPE) {
        val model = service.findById(id)
        service.delete(model)
        ctx.status(HTTP_STATUS_NO_CONTENT_204)
    }
    companion object {
        const val HTTP_STATUS_OK_200 = 200
        const val HTTP_STATUS_CREATED_201 = 201
        const val HTTP_STATUS_NO_CONTENT_204 = 204
    }
}