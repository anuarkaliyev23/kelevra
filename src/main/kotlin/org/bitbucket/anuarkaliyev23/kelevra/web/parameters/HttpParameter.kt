package org.bitbucket.anuarkaliyev23.kelevra.web.parameters

interface HttpParameter {
    val parameterName: String
}