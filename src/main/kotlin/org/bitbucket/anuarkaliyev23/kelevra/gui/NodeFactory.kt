package org.bitbucket.anuarkaliyev23.kelevra.gui

import javafx.scene.Node

interface NodeFactory {
    fun node(): Node
}