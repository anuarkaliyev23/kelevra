//package org.bitbucket.anuarkaliyev23.kelevra.gui
//
//import javafx.collections.FXCollections
//import javafx.scene.Node
//import javafx.scene.control.Button
//import javafx.scene.control.ComboBox
//import javafx.scene.layout.VBox
//import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
//import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.AccountService
//import org.controlsfx.control.ToggleSwitch
//import org.controlsfx.control.textfield.TextFields
//
//class AccountFormFactory(
//        val service: AccountService
//) : NodeFactory {
//    override fun node(): Node {
//        val idField = TextFields.createClearableTextField()
//        val descriptionField = TextFields.createClearableTextField()
//        val activeField = ToggleSwitch(Account::active.name)
//
//        val parentComboBox = ComboBox(
//                FXCollections.observableArrayList(service.listAll())
//        )
//
//        val button = Button("Save").apply {
//            setOnAction {
//                val account = Account(idField.text.trim(), descriptionField.text.trim(), activeField.isSelected, parentComboBox.value)
//                service.dao.save(account)
//            }
//            maxWidth = Double.MAX_VALUE
//        }
//
//        return VBox(
//                idField,
//                descriptionField,
//                activeField,
//                parentComboBox,
//                button
//        )
//
//    }
//
//}