//package org.bitbucket.anuarkaliyev23.kelevra.gui
//
//import javafx.beans.property.SimpleStringProperty
//import javafx.collections.FXCollections
//import javafx.scene.Node
//import javafx.scene.control.TableColumn
//import javafx.scene.control.TableView
//import javafx.scene.control.cell.PropertyValueFactory
//import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
//import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.AccountService
//
//class AccountTableFactory(
//        val service: AccountService
//): NodeFactory {
//    override fun node(): Node {
//        val accounts = FXCollections.observableArrayList(service.listAll())
//        val table = TableView<Account>()
//
//        val idColumn = TableColumn<Account, Long>(Account::id.name).apply {
//            cellValueFactory = PropertyValueFactory(Account::id.name)
//        }
//
//        val descriptionColumn = TableColumn<Account, String>(Account::description.name).apply {
//            cellValueFactory = PropertyValueFactory(Account::description.name)
//        }
//
//        val activeColumn = TableColumn<Account, Boolean>(Account::active.name).apply {
//            cellValueFactory = PropertyValueFactory(Account::active.name)
//        }
//
//        val parentColumn = TableColumn<Account, Account>(Account::parent.name)
//
//        val parentIdColumn = TableColumn<Account, String>(Account::id.name).apply {
//            setCellValueFactory { SimpleStringProperty(it.value.parent?.id) }
//        }
//
//        val parentDescriptionColumn = TableColumn<Account, String>(Account::description.name).apply {
//            setCellValueFactory { SimpleStringProperty(it.value.parent?.description) }
//        }
//
//        val parentActiveColumn = TableColumn<Account, String>(Account::active.name).apply {
//            setCellValueFactory { SimpleStringProperty(it.value.parent?.active?.toString() ?: "") }
//        }
//
//        parentColumn.columns.addAll(parentIdColumn, parentDescriptionColumn, parentActiveColumn)
//        table.columns.addAll(idColumn, descriptionColumn, activeColumn, parentColumn)
//        table.items = accounts
//        return table
//    }
//}