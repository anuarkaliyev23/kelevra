//package org.bitbucket.anuarkaliyev23.kelevra.gui
//
//import javafx.application.Application
//import javafx.scene.Scene
//import javafx.scene.layout.StackPane
//import javafx.stage.Stage
//import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.AccountServiceSimple
//
//class FxGUI : Application() {
//    override fun start(stage: Stage) {
//        val scene = Scene(StackPane(AccountFormFactory(AccountServiceSimple).node()))
//        stage.scene = scene
//
//        stage.title = "Test"
//        stage.show()
//    }
//}