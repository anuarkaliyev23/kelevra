package org.bitbucket.anuarkaliyev23.kelevra.exceptions

open class KelevraException(msg: String) : Exception(msg)