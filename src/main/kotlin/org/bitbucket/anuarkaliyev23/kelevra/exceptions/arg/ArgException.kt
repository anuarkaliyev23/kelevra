package org.bitbucket.anuarkaliyev23.kelevra.exceptions.arg

import org.bitbucket.anuarkaliyev23.kelevra.exceptions.KelevraException

open class ArgException(msg: String) : KelevraException(msg)