package org.bitbucket.anuarkaliyev23.kelevra.exceptions.web

import org.bitbucket.anuarkaliyev23.kelevra.exceptions.KelevraException

open class WebException(msg: String, val statusCode: Int) : KelevraException(msg)