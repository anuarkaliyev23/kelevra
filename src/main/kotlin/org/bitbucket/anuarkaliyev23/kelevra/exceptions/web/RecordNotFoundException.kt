package org.bitbucket.anuarkaliyev23.kelevra.exceptions.web

class RecordNotFoundException : WebException("Record wasn't found", 404)