package org.bitbucket.anuarkaliyev23.kelevra.exceptions.arg

class UnrecognizedArgException(arg: String) : ArgException("$arg was not recognized")