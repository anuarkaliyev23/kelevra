package org.bitbucket.anuarkaliyev23.kelevra

import org.bitbucket.anuarkaliyev23.kelevra.configuration.DatabaseUtils

fun main() {
    DatabaseUtils.startDatabase()
}