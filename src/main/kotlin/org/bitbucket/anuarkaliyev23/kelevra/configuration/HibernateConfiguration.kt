package org.bitbucket.anuarkaliyev23.kelevra.configuration

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import org.hibernate.SessionFactory
import org.hibernate.boot.Metadata
import org.hibernate.boot.MetadataSources
import org.hibernate.boot.registry.StandardServiceRegistryBuilder

object HibernateConfiguration {
    val sessionFactory: SessionFactory
    val metadata: Metadata
    init {
        val standardServiceRegistry = StandardServiceRegistryBuilder()
                .configure()
                .build()
        val metadataSource = MetadataSources(standardServiceRegistry)
        metadataSource.addAnnotatedClass(Account::class.java)
        metadataSource.addAnnotatedClass(Wiring::class.java)
        metadataSource.addAnnotatedClass(GenericWiringParty::class.java)
        metadata = metadataSource.buildMetadata()
        sessionFactory = metadata.buildSessionFactory()
    }

}