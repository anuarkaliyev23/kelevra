package org.bitbucket.anuarkaliyev23.kelevra.configuration


import org.bitbucket.anuarkaliyev23.kelevra.logger
import org.h2.tools.Server

object DatabaseUtils {
    private const val DATABASE_PORT = 9092
    private const val DATABASE_NAME = "kelevra"
    private const val DATABASE_USERNAME = "kelevra"
    private const val DATABASE_PASSWORD = ""

    var server: Server? = null

    fun startDatabase() {
        server = Server.createTcpServer("-tcp", "-tcpAllowOthers", "-ifNotExists", "-webPort", DATABASE_PORT.toString()).start()
        logger().info("H2 Database has started at port $DATABASE_PORT")
    }

    fun stopDatabase() {
        server?.stop()
        logger().info("H2 Database has been stopped")
    }
}

