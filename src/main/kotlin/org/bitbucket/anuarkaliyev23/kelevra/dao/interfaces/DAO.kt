package org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces

import java.io.Serializable

interface DAO<T> {
    fun findAll() : List<T>
    fun delete(obj: T)
    fun update(obj: T)
    fun save(obj: T)
}