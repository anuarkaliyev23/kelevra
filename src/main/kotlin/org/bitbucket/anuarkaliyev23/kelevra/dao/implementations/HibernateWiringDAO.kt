package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations

import org.bitbucket.anuarkaliyev23.kelevra.configuration.HibernateConfiguration
import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.WiringDAO
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import java.time.LocalDate

object HibernateWiringDAO : GenericHibernateDAO<Wiring>(), WiringDAO {
    override fun findByDebit(account: Account): List<Wiring> {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()
        val records = findByFieldValue(Wiring.DB_TABLE_NAME, Wiring.DB_DEBIT_FIELD_NAME, account, session)
        transaction.commit()
        session.close()

        return records
    }

    override fun findByCredit(account: Account): List<Wiring> {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()
        val records = findByFieldValue(Wiring.DB_TABLE_NAME, Wiring.DB_CREDIT_FIELD_NAME, account, session)
        transaction.commit()
        session.close()

        return records
    }

    override fun findBySender(sender: GenericWiringParty): List<Wiring> {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()
        val records = findByFieldValue(Wiring.DB_TABLE_NAME, Wiring.DB_SENDER_FIELD_NAME, sender, session)
        transaction.commit()
        session.close()

        return records
    }

    override fun findByReceiver(receiver: GenericWiringParty): List<Wiring> {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()
        val records = findByFieldValue(Wiring.DB_TABLE_NAME, Wiring.DB_RECEIVER_FIELD_NAME, receiver, session)
        transaction.commit()
        session.close()

        return records
    }

    override fun findAny(wiringPartyEntity: GenericWiringParty): List<Wiring> {
        return findBySender(wiringPartyEntity) + findByReceiver(wiringPartyEntity)
    }

    override fun findAny(account: Account): List<Wiring> {
        return findByDebit(account) + findByCredit(account)
    }

    override fun findByDate(date: LocalDate): List<Wiring> {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()
        val records = findByFieldValue(Wiring.DB_TABLE_NAME, Wiring.DB_DATE_FIELD_NAME, date, session)
        transaction.commit()
        session.close()

        return records
    }

    override fun between(inclusive: LocalDate, exclusive: LocalDate): List<Wiring> {
        return findAll().filter { it.date >= inclusive && it.date < exclusive }
    }

    override fun lastEntry(): Wiring? {
        return findAll().maxBy { it.date }
    }

    override fun findById(id: Int): Wiring? {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()
        val record = findById(id, session, Wiring::class.java)
        transaction.commit()
        session.close()

        return record
    }

    override fun findAll(): List<Wiring> {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()
        val records = listAll(Wiring::class.java, session)
        transaction.commit()
        session.close()

        return records
    }
}