package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations

import org.bitbucket.anuarkaliyev23.kelevra.configuration.HibernateConfiguration
import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.AccountDAO
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.hibernate.Hibernate
import org.hibernate.Session
import org.hibernate.query.Query
import java.io.Serializable

object HibernateAccountDAO : GenericHibernateDAO<Account>(), AccountDAO {

    override fun findAll() : List<Account> {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()

        val list = listAll(Account::class.java, session)

        transaction.commit()
        session.close()
        return list
    }


    override fun findById(id: String): Account? {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()

        val account = findById(id, session, Account::class.java)

        transaction.commit()
        session.close()
        return account
    }


}