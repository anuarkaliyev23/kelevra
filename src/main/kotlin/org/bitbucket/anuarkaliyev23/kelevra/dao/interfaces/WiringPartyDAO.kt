package org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty

interface WiringPartyDAO : DAO<GenericWiringParty> {
    fun findById(id: String) : GenericWiringParty?
}