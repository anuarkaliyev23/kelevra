package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations

import org.bitbucket.anuarkaliyev23.kelevra.configuration.HibernateConfiguration
import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.WiringPartyDAO
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty

object HibernateWiringPartyDAO : WiringPartyDAO, GenericHibernateDAO<GenericWiringParty>() {
    override fun findById(id: String): GenericWiringParty? {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()

        val record = findById(id, session, GenericWiringParty::class.java)

        transaction.commit()
        session.close()

        return record
    }

    override fun findAll(): List<GenericWiringParty> {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()

        val records = listAll(GenericWiringParty::class.java, session)

        transaction.commit()
        session.close()

        return records
    }


}