package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations

import org.bitbucket.anuarkaliyev23.kelevra.configuration.HibernateConfiguration
import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.DAO
import org.hibernate.Session
import java.io.Serializable

abstract class GenericHibernateDAO<T> : DAO<T> {

    protected fun listAll (type: Class<T>, session: Session) : List<T> {
        val criteriaBuilder = session.criteriaBuilder
        val query = criteriaBuilder.createQuery(type)
        query.from(type)
        return session.createQuery(query).resultList
    }

    protected fun findById(id: Serializable, session: Session, type: Class<T>) : T? {
        val record = session.get(type, id)
        return record
    }

    protected fun findByFieldValue(tableName: String, fieldName: String, fieldValue: Any, session: Session) : List<T> {
        val query = session.createQuery("SELECT e FROM $tableName e WHERE e.${fieldName} = :value")
        query.setParameter("value", fieldValue)
        val resultList = query.resultList
        return resultList as List<T>
    }

    protected fun save(obj: T, session: Session) {
        session.save(obj)
    }

    protected fun delete(obj: T, session: Session) {
        session.delete(obj)
    }

    protected fun update(obj: T, session: Session) {
        session.update(obj)
    }

    override fun save(obj: T) {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()

        save(obj, session)

        transaction.commit()
        session.close()
    }

    override fun delete(obj: T) {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()

        delete(obj, session)

        transaction.commit()
        session.close()
    }

    override fun update(obj: T) {
        val session = HibernateConfiguration.sessionFactory.openSession()
        val transaction = session.beginTransaction()

        update(obj, session)

        transaction.commit()
        session.close()
    }



}