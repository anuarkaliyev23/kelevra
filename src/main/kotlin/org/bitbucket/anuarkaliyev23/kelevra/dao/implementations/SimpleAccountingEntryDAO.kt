//package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations
//
//import com.j256.ormlite.dao.BaseDaoImpl
//import com.j256.ormlite.jdbc.JdbcConnectionSource
//import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.AccountingEntryDAO
//import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
//import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
//import java.time.LocalDate
//
//class SimpleAccountingEntryDAO(val connectionSource: JdbcConnectionSource) :
//        BaseDaoImpl<Wiring, Int>(connectionSource, Wiring::class.java),
//        AccountingEntryDAO {
//    override fun between(inclusive: LocalDate, exclusive: LocalDate): List<Wiring> {
//        val entries = queryForAll()
//        return entries.filter { it.date >= inclusive && it.date < exclusive }
//    }
//
//    override fun findByDebit(account: Account): List<Wiring> {
//        return this.queryBuilder().where().eq(Wiring.DB_DEBIT_FIELD_NAME, account.id).query()
//    }
//
//    override fun findByCredit(account: Account): List<Wiring> {
//        return this.queryBuilder().where().eq(Wiring.DB_CREDIT_FIELD_NAME, account.id).query()
//    }
//
//    override fun findAny(account: Account): List<Wiring> {
//        return this.queryBuilder()
//                .where()
//                .eq(Wiring.DB_DEBIT_FIELD_NAME, account.id)
//                .or()
//                .eq(Wiring.DB_CREDIT_FIELD_NAME, account.id)
//                .query()
//    }
//
//    override fun findByDate(date: LocalDate): List<Wiring> {
//        val entries = queryForAll()
//        return entries.filter { it.date == date}
//    }
//
//    override fun lastEntry() : Wiring {
//        val entries = queryForAll()
//        return entries.last()
//    }
//
//}