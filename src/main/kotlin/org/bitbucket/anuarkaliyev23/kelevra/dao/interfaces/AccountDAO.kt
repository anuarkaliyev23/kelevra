package org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import java.io.Serializable

interface AccountDAO: DAO<Account> {
    fun findById(id: String): Account?
}