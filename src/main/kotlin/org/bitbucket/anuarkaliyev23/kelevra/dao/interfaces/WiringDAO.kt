package org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces

import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import java.time.LocalDate

interface WiringDAO: DAO<Wiring> {
    fun findByDebit(account: Account) : List<Wiring>
    fun findByCredit(account: Account) : List<Wiring>
    fun findBySender(sender: GenericWiringParty): List<Wiring>
    fun findByReceiver(receiver: GenericWiringParty): List<Wiring>
    fun findAny(wiringPartyEntity: GenericWiringParty): List<Wiring>
    fun findAny(account: Account) : List<Wiring>
    fun findByDate(date: LocalDate) : List<Wiring>
    fun between(inclusive: LocalDate, exclusive: LocalDate) : List<Wiring>
    fun lastEntry() : Wiring?
    fun findById(id: Int) : Wiring?
}