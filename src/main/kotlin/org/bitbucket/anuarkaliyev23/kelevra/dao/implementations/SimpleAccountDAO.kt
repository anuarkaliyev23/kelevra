//package org.bitbucket.anuarkaliyev23.kelevra.dao.implementations
//
//import com.j256.ormlite.dao.BaseDaoImpl
//import com.j256.ormlite.jdbc.JdbcConnectionSource
//import org.bitbucket.anuarkaliyev23.kelevra.dao.interfaces.AccountDAO
//import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
//
//class SimpleAccountDAO(val connectionSource: JdbcConnectionSource) :
//        BaseDaoImpl<Account, Int>(connectionSource, Account::class.java),
//        AccountDAO {
//    override fun findByCode(code: String): Account? {
//        return queryBuilder().where().eq(Account.DB_CODE_FIELD_NAME, code).queryForFirst()
//    }
//}