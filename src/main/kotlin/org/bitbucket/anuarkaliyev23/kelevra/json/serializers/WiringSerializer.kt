package org.bitbucket.anuarkaliyev23.kelevra.json.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring

class WiringSerializer : StdSerializer<Wiring>(Wiring::class.java) {
    override fun serialize(wiring: Wiring, generator: JsonGenerator, provider: SerializerProvider) {
        generator.writeStartObject()
        generator.writeStringField(Wiring::id.name, wiring.id.toString())
        generator.writeObjectField(Wiring::debit.name, wiring.debit)
        generator.writeObjectField(Wiring::credit.name, wiring.credit)
        generator.writeObjectField(Wiring::sender.name, wiring.sender)
        generator.writeObjectField(Wiring::receiver.name, wiring.receiver)
        generator.writeNumberField(Wiring::amount.name, wiring.amount)
        generator.writeStringField(Wiring::date.name, wiring.date.toString())
        generator.writeStringField(Wiring::note.name, wiring.note)
        generator.writeObjectField(Wiring::meta.name, wiring.meta)
        generator.writeEndObject()
    }
}