package org.bitbucket.anuarkaliyev23.kelevra.json.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty

class GenericWiringPartySerializer : StdSerializer<GenericWiringParty>(GenericWiringParty::class.java) {
    override fun serialize(wiringParty: GenericWiringParty, generator: JsonGenerator, provider: SerializerProvider) {
        generator.writeStartObject()
        generator.writeStringField(wiringParty::id.name, wiringParty.id)
        generator.writeStringField(wiringParty::description.name, wiringParty.description)
        generator.writeObjectField(wiringParty::meta.name, wiringParty.meta)
        generator.writeEndObject()
    }
}