package org.bitbucket.anuarkaliyev23.kelevra.json.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.ModelMetaInfo

class ModelMetaInfoSerializer : StdSerializer<ModelMetaInfo>(ModelMetaInfo::class.java) {
    override fun serialize(meta: ModelMetaInfo, generator: JsonGenerator, provider: SerializerProvider) {
        generator.writeStartObject()
        generator.writeStringField(meta::createdAt.name, meta.createdAt.toString())
        generator.writeEndObject()
    }

}