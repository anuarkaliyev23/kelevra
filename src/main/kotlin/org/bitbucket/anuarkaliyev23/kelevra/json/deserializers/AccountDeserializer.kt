package org.bitbucket.anuarkaliyev23.kelevra.json.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.node.NullNode
import org.bitbucket.anuarkaliyev23.kelevra.logger
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.AccountServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.ModelService

class AccountDeserializer(private val service: ModelService<Account, String> = AccountServiceSimple) : StdDeserializer<Account>(Account::class.java) {
    override fun deserialize(parser: JsonParser, context: DeserializationContext): Account {
        val node : JsonNode = parser.codec.readTree<JsonNode>(parser)
        val id = node[Account::id.name].asText()
        val description = node[Account::description.name].asText()
        val active = node[Account::active.name].asBoolean()
        val parentId = if (node[Account::parent.name] is NullNode) null else node[Account::parent.name].asText()

        logger().debug("deserialized $node as Account with id = $id, description = $description, active = $active, parent = $parentId")
        val parent = if (parentId != null) service.findById(parentId) else null
        return Account(id, description, active, parent)
    }
}