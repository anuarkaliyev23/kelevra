package org.bitbucket.anuarkaliyev23.kelevra.json.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import org.bitbucket.anuarkaliyev23.kelevra.logger
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.AccountServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.WiringPartyServiceSimple
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.AccountService
import org.bitbucket.anuarkaliyev23.kelevra.services.interfaces.WiringPartyService
import java.time.LocalDate

class WiringDeserializer(
        val accountService: AccountService = AccountServiceSimple,
        val wiringPartyService: WiringPartyService = WiringPartyServiceSimple
) : StdDeserializer<Wiring>(Wiring::class.java) {
    override fun deserialize(parser: JsonParser, context: DeserializationContext): Wiring {
        val node = parser.codec.readTree<JsonNode>(parser)
        val debitId = node[Wiring::debit.name].asText()
        val creditId = node[Wiring::credit.name].asText()
        val amount = node[Wiring::amount.name].asLong()
        val dateString = node[Wiring::date.name].asText()
        val note = node[Wiring::note.name].asText()
        val senderId = node[Wiring::sender.name].asText()
        val receiverId = node[Wiring::receiver.name].asText()

        logger().debug("Parsed $node into debit = $debitId, credit = $creditId, amount = $amount, date = $dateString, note = $note, sender = $senderId, receiver = $receiverId")
        val debit = accountService.findById(debitId)
        val credit = accountService.findById(creditId)

        val sender = wiringPartyService.findById(senderId)
        val receiver = wiringPartyService.findById(receiverId)

        val date = LocalDate.parse(dateString)
        return Wiring(0, debit, credit, sender, receiver, amount, date, note)
    }

}