package org.bitbucket.anuarkaliyev23.kelevra.json.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import org.bitbucket.anuarkaliyev23.kelevra.model.TurnoverBalanceSheet

class TurnoverBalanceSheetSerializer : StdSerializer<TurnoverBalanceSheet>(TurnoverBalanceSheet::class.java) {
    override fun serialize(turnoverBalanceSheet: TurnoverBalanceSheet, generator: JsonGenerator, provider: SerializerProvider) {
        generator.writeStartObject()

        generator.writeObjectField(turnoverBalanceSheet::account.name, turnoverBalanceSheet.account)
        generator.writeObjectField(turnoverBalanceSheet::entries.name, turnoverBalanceSheet.entries)
        generator.writeNumberField(turnoverBalanceSheet::startBalance.name, turnoverBalanceSheet.startBalance)
        generator.writeNumberField(turnoverBalanceSheet::totalBalance.name, turnoverBalanceSheet.totalBalance())
        generator.writeStringField("earliestEntry", turnoverBalanceSheet.startDate().toString())
        generator.writeStringField("oldestEntry", turnoverBalanceSheet.endDate().toString())

        generator.writeEndObject()
    }
}