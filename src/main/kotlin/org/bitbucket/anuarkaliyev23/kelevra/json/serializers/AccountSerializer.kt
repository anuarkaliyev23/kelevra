package org.bitbucket.anuarkaliyev23.kelevra.json.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account

class AccountSerializer : StdSerializer<Account>(Account::class.java) {
    override fun serialize(account: Account, generator: JsonGenerator, provider: SerializerProvider) {
        generator.writeStartObject()
        generator.writeStringField(Account::id.name, account.id)
        generator.writeStringField(Account::description.name, account.description)
        generator.writeBooleanField(Account::active.name, account.active)
        generator.writeStringField(Account::parent.name, account.parent?.id)
        generator.writeObjectField(Account::meta.name, account.meta)
        generator.writeEndObject()
    }
}