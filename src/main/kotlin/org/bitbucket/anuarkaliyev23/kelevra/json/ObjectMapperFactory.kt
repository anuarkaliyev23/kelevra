package org.bitbucket.anuarkaliyev23.kelevra.json

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.bitbucket.anuarkaliyev23.kelevra.json.deserializers.AccountDeserializer
import org.bitbucket.anuarkaliyev23.kelevra.json.deserializers.GenericWiringPartyDeserializer
import org.bitbucket.anuarkaliyev23.kelevra.json.deserializers.WiringDeserializer
import org.bitbucket.anuarkaliyev23.kelevra.json.serializers.*
import org.bitbucket.anuarkaliyev23.kelevra.model.TurnoverBalanceSheet
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Account
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.ModelMetaInfo
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.Wiring
import org.bitbucket.anuarkaliyev23.kelevra.services.implementations.AccountServiceSimple

object ObjectMapperFactory {
    fun objectMapper(): ObjectMapper {
        val module = SimpleModule()
                .addSerializer(Account::class.java, AccountSerializer())
                .addSerializer(GenericWiringParty::class.java, GenericWiringPartySerializer())
                .addSerializer(ModelMetaInfo::class.java, ModelMetaInfoSerializer())
                .addSerializer(Wiring::class.java, WiringSerializer())
                .addSerializer(TurnoverBalanceSheet::class.java, TurnoverBalanceSheetSerializer())
                .addDeserializer(Account::class.java, AccountDeserializer())
                .addDeserializer(GenericWiringParty::class.java, GenericWiringPartyDeserializer())
                .addDeserializer(Wiring::class.java, WiringDeserializer())
        val mapper = ObjectMapper()
                .registerKotlinModule()
                .registerModule(module)
        return mapper
    }
}