package org.bitbucket.anuarkaliyev23.kelevra.json.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import org.bitbucket.anuarkaliyev23.kelevra.logger
import org.bitbucket.anuarkaliyev23.kelevra.model.persistence.GenericWiringParty

class GenericWiringPartyDeserializer : StdDeserializer<GenericWiringParty>(GenericWiringParty::class.java) {
    override fun deserialize(parser: JsonParser, context: DeserializationContext): GenericWiringParty {
        val node = parser.codec.readTree<JsonNode>(parser)
        val id = node[GenericWiringParty::id.name].asText()
        val description = node[GenericWiringParty::description.name].asText()

        logger().debug("Parsed $node into id = $id, description = $description")
        return GenericWiringParty(id, description)
    }

}